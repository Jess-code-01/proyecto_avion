
public class AvionPasajeros extends Avion {
	
	// Atributos
	private int pasajeros;

	/**
	 * Constructor
	 * @param color
	 * @param tamanio
	 */
	public AvionPasajeros(String color, int tamanio, int pasajeros) {
		super(color, tamanio);
		this.pasajeros = pasajeros;
	}

	//--------Métodos Getters and Setters------------------------>
	public int getPasajeros() {
		return pasajeros;
	}

	public void setPasajeros(int pasajeros) {
		this.pasajeros = pasajeros;
	}
	//-----------------------------------------------------------||
	
	public void servir() {
		System.out.println("Sirviendo...");
	}
	
}
