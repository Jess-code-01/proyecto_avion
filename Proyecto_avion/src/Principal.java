
public class Principal {
	public static void main(String[] args) {
		
		// Construcción de un avión de carga
		AvionCarga avionCarga = new AvionCarga("Azul", 800);
		avionCarga.cargar();
		avionCarga.despegar();
		avionCarga.aterrizar();
		avionCarga.frenar();
		avionCarga.descargar();
		
		System.out.println("------------------Avión Pasajeros-------------------");
		
		AvionPasajeros avionPasajeros = new AvionPasajeros("Verde", 900, 250);
		avionPasajeros.despegar();
		avionPasajeros.servir();
		avionPasajeros.aterrizar();
		avionPasajeros.frenar();

		System.out.println("------------------Avión Militar-------------------");
		
		AvionMilitar avionMilitar = new AvionMilitar("Verde", 900, 6);
		avionMilitar.detectarAmenaza(false);
		
		for (int i = 0; i < 8; i++) {
			avionMilitar.detectarAmenaza(true);			
		}
	}
}
