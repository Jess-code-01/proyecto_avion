
public class AvionMilitar extends Avion {
	
	private int misiles;

	public AvionMilitar(String color, int tamanio, int misiles) {
		super(color, tamanio);
		this.misiles = misiles;
	}

	public int getMisiles() {
		return misiles;
	}
	public void setMisiles(int misiles) {
		this.misiles = misiles;
	}
	
	public void detectarAmenaza(boolean amenaza) {
		if(amenaza) disparar();
		else System.out.println("No es una amenaza");
	}
	
	private void disparar() { 
		if(misiles > 0) {
			System.out.println("Disparando...");
			--misiles;			
		} else 
			System.out.println("No hay munisiones");
		
	}
	
	
}
