
/**
 * Fecha: 06 de Julio de 2021
 * @author Jessica Johana Ospina
 * Empresa: UTP - MINTIC
 * Proyecto: Fábrica de aviones
 * Clase: Clase que representa un avión
 *
 */
public class Avion {
	
	// Atributos
	private String color;
	private int tamanio;
	
	//Constructor
	public Avion(String color, int tamanio) {
		this.color = color;
		this.tamanio = tamanio;
	}
	
	public Avion() {
		this.color = "";
		this.tamanio = 0;
	}
	
	//-------------------Métodos Getters and Setters----------------------->
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public int getTamanio() {
		return tamanio;
	}
	
	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}
	//---------------------------------------------------------------------||

	//-----------------------Métodos---------------------------------------->
	public void aterrizar() {
		System.out.println("Aterrizando...");
	}
	
	public void despegar() {
		System.out.println("Despegando...");
	}
	
	public void frenar() {
		System.out.println("Frenando...");
	}
	//---------------------------------------------------------------------||

}

