
public class AvionCarga extends Avion {

	// Constructor
	public AvionCarga(String color, int tamanio) {
		super(color, tamanio);
	}
	
	//Métodos
	public void cargar() {
		System.out.println("Cargando...");
	}
	public void descargar() {
		System.out.println("Descargando...");
	}
	
}
